import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.*;

import java.awt.*;

public class ExitPoint extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4180691197121728549L;
	private String[] hosts;
	private int[] ports;
	
	private int availableNums;
	private DatagramSocket socket;
	
	private JTextField availableIndicator;
	private JButton carEnterButton;
	private void setAvailableIndicator(int i) {
		availableIndicator.setText(Integer.toString(i));
	}
	
	private void setupViews() {
		Container contentPane= getContentPane();

		JPanel enterPanel = new JPanel();
		enterPanel.add(new JLabel("我是一个出口"),BorderLayout.WEST);
		
		contentPane.add(enterPanel);
		
		carEnterButton = new JButton("出车");
		carEnterButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				synchronized(this){
					Message message = new Message();
					message.type = MessageType.CAR_OUT;
					message.sequence = 1;
					message.source = 2;
					message.extra = "test case";
					
					for (int i = 0; i < hosts.length; i++) {
						try {
							String str = message.toString();
							DatagramPacket sendPacket = new DatagramPacket(str.getBytes(), str.length(), InetAddress.getByName(hosts[i]), ports[i]);
							socket.send(sendPacket);
							
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});
		contentPane.add(carEnterButton,BorderLayout.SOUTH);	

		this.setTitle("出口");
		this.setSize(300,100);
		this.setResizable(false);
	}
	
	public ExitPoint()
	{
		setupViews();
	}
	
	public void config(String[] hosts, int[] ports) {
		this.hosts = hosts;
		this.ports = ports;
	}
	
	public void start() throws SocketException {
		socket = new DatagramSocket(); 	
		this.show();
	}
}
