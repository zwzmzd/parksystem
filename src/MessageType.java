public enum MessageType { 
	CAR_OUT,
	CAR_IN,
	CAT_IN_REPLY,
	CRITICAL_AREA_ACQUIRE,
	CRITICAL_AREA_REPLY,
	LOCAL_I_WANT_ACQUIRE_CRITICAL_AREA
}