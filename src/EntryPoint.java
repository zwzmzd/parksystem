import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class EntryPoint extends JDialog implements Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6939622902110941592L;
	private String[] hosts;
	private int[] ports;
	private String myHost;
	private int myPort;
	private int availableNums;
	private int otherEntryCount;

	private int no;

	private DatagramSocket socket;

	private JTextField availableIndicator;
	private JButton carEnterButton;

	private void setAvailableIndicator(int i) {
		availableIndicator.setText("" + i);
	}

	private void setupViews() {
		Container contentPane = getContentPane();

		JPanel enterPanel = new JPanel();
		enterPanel.add(new JLabel("可用车位:"), BorderLayout.WEST);

		availableIndicator = new JTextField();
		availableIndicator.setPreferredSize(new Dimension(60, 24));
		enterPanel.add(availableIndicator, BorderLayout.EAST);
		contentPane.add(enterPanel);

		carEnterButton = new JButton("进车");
		carEnterButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (this) {
					carEnterButton.setEnabled(false);
					carEnterButton.setText("请等待");
					Message request = new Message();
					request.type = MessageType.LOCAL_I_WANT_ACQUIRE_CRITICAL_AREA;
					request.sequence = 0;

					try {
						String str = request.toString();
						DatagramPacket sendPacket = new DatagramPacket(str
								.getBytes(), str.length(), InetAddress
								.getByName(myHost), myPort);
						socket.send(sendPacket);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		contentPane.add(carEnterButton, BorderLayout.SOUTH);

		this.setTitle("入口");
		this.setSize(300, 100);
		this.setResizable(false);
	}

	public EntryPoint() {
		setupViews();
	}

	public void config(String[] hosts, int[] ports, String myHost, int myPort,
			int systemCapacity, int otherEntryCount, int no) {
		this.hosts = hosts;
		this.ports = ports;
		this.myHost = myHost;
		this.myPort = myPort;

		this.no = no;
		this.otherEntryCount = otherEntryCount;

		this.availableNums = systemCapacity;
		setAvailableIndicator(systemCapacity);
	}

	public void start() throws SocketException {
		socket = new DatagramSocket(this.myPort);
		Thread t = new Thread(this);
		t.start();

		this.show();
	}

	public void run() {
		int waitListNum = 0;
		DatagramPacket packet = new DatagramPacket(new byte[1000], 1000);

		String[] hostQueue = new String[this.otherEntryCount];
		int[] portQueue = new int[this.otherEntryCount];
		int count = 0;

		int T = 1;
		int lastRequestSeq = -1;
		
		boolean iAmInCriticalArea = false;

		while (true) {
			try {
				socket.receive(packet);
				Message message = Message.fromString(new String(packet
						.getData(), 0, packet.getLength()));

				T = Math.max(T, message.sequence);
				Message reply = new Message();
				reply.source = this.no;
				reply.sequence = ++T;

				if (message.type == MessageType.CAR_OUT) {
					modifyAvailableNums(+1);
					
					// 把持临界区的节点才有这个待遇，否则考虑以下情况：
					// 一个节点A在等待临界区，它记录的停车位为0，拥有临界区的节点B值为-1
					// 这时一个出口节点释放了一辆车，向所有节点发送+1的包，但是这个包到达A比较慢，先到达了B，B给所有人发送了CAR_OUT的包。之后A才收到+1的包
					// A的值变化为0-->-1-->0，此时它不应进入下面代码，否则乱套了
					if (iAmInCriticalArea && this.availableNums == 0) {
						System.out.println(this.no + " release success");
						for (int i = 0; i < hosts.length; i++) {
							reply.type = MessageType.CAR_IN;
							String str = reply.toString();
							DatagramPacket sendPacket = new DatagramPacket(
									str.getBytes(), str.length(),
									InetAddress.getByName(hosts[i]), ports[i]);
							socket.send(sendPacket);
						}
					}
				} else if (message.type == MessageType.CAR_IN) {
					modifyAvailableNums(-1);

					reply.type = MessageType.CAT_IN_REPLY;
					String str = reply.toString();
					DatagramPacket sendPacket = new DatagramPacket(
							str.getBytes(), str.length(),
							InetAddress.getByName(packet.getAddress()
									.getHostAddress()), packet.getPort());
					socket.send(sendPacket);
				} else if (message.type == MessageType.CAT_IN_REPLY) {
					waitListNum--;
					if (waitListNum == 0) {
						reply.type = MessageType.CRITICAL_AREA_REPLY;
						String str = reply.toString();

						for (int i = 0; i < count; i++) {
							DatagramPacket sendPacket = new DatagramPacket(
									str.getBytes(), str.length(),
									InetAddress.getByName(hostQueue[i]),
									portQueue[i]);
							socket.send(sendPacket);
						}
						count = 0;
						lastRequestSeq = -1;

						iAmInCriticalArea = false;
						carEnterButton.setEnabled(true);
						carEnterButton.setText("进车");
					}
				} else if (message.type == MessageType.CRITICAL_AREA_ACQUIRE) {
					boolean happenBefore = lastRequestSeq < 0
							|| message.sequence < lastRequestSeq
							|| (message.sequence == lastRequestSeq && message.source < this.no);
					if (iAmInCriticalArea || !happenBefore) {
						hostQueue[count] = packet.getAddress().getHostAddress();
						portQueue[count] = packet.getPort();
						count++;
					} else {
						reply.type = MessageType.CRITICAL_AREA_REPLY;
						String str = reply.toString();
						DatagramPacket sendPacket = new DatagramPacket(
								str.getBytes(), str.length(),
								InetAddress.getByName(packet.getAddress()
										.getHostAddress()), packet.getPort());
						socket.send(sendPacket);
					}
				} else if (message.type == MessageType.CRITICAL_AREA_REPLY) {
					waitListNum--;
					if (waitListNum == 0) {
						System.out.println("entry " + this.no + " has access.");
						iAmInCriticalArea = true;
						waitListNum = otherEntryCount;

						modifyAvailableNums(-1);
						if (this.availableNums >= 0) {
							for (int i = 0; i < hosts.length; i++) {
								reply.type = MessageType.CAR_IN;
								String str = reply.toString();
								DatagramPacket sendPacket = new DatagramPacket(
										str.getBytes(), str.length(),
										InetAddress.getByName(hosts[i]),
										ports[i]);
								socket.send(sendPacket);
							}
						} else {
							System.out.println(this.no
									+ " Please wait for release");
						}
					}
				} else if (message.type == MessageType.LOCAL_I_WANT_ACQUIRE_CRITICAL_AREA) {
					for (int i = 0; i < hosts.length; i++) {
						reply.type = MessageType.CRITICAL_AREA_ACQUIRE;
						String str = reply.toString();
						DatagramPacket sendPacket = new DatagramPacket(
								str.getBytes(), str.length(),
								InetAddress.getByName(hosts[i]), ports[i]);
						socket.send(sendPacket);
					}

					waitListNum = otherEntryCount;
					lastRequestSeq = reply.sequence;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private synchronized void modifyAvailableNums(int offset) {
		availableNums += offset;
		setAvailableIndicator(availableNums);
	}
}
