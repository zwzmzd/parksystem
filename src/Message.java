import java.io.*;

public class Message implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5687990831494472256L;
	public MessageType type;
	public int sequence;
	public int source;
	public String extra = "";
	
	Message() {
	}
	
	public String toString() {
		return type.ordinal() + "$$" + sequence + "$$" + source + "$$" + extra + "$$";
	}
	
	public static Message fromString(String raw) {
		Message message = new Message();
		String[] components = raw.split("\\$\\$", -1);
		
		message.type = MessageType.values()[Integer.parseInt(components[0])];
		message.sequence = Integer.parseInt(components[1]);
		message.source = Integer.parseInt(components[2]);
		message.extra = components[3];
		
		return message;
	}
}
