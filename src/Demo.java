import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.SocketException;


public class Demo {

	public static void main(String[] args) {
		Toolkit kit = Toolkit.getDefaultToolkit(); // 定义工具包
		Dimension screenSize = kit.getScreenSize(); // 获取屏幕的尺寸s
		int screenHeight = screenSize.height; // 获取屏幕的高
		int baseX = 0;
		int baseY = 0;

		String[] hosts = new String[] { "localhost", "localhost", "localhost",
				"localhost", "localhost", "localhost", "localhost", "localhost" };
		int[] ports = new int[] { 10000, 10001, 10002, 10003, 10004, 10005,
				10006, 10007 };
		int N = hosts.length;
		for (int i = 0; i < N; i++) {
			EntryPoint point = new EntryPoint();

			String[] h = new String[N - 1];
			for (int j = 0; j < hosts.length; j++) {
				if (j < i) {
					h[j] = hosts[j];
				} else if (j > i) {
					h[j - 1] = hosts[j];
				}
			}
			int[] p = new int[N - 1];
			for (int j = 0; j < hosts.length; j++) {
				if (j < i) {
					p[j] = ports[j];
				} else if (j > i) {
					p[j - 1] = ports[j];
				}
			}

			point.config(h, p, hosts[i], ports[i], 2, N - 1, i);

			int width = point.getWidth() + 10;
			int height = point.getHeight() + 10;
			if (baseY + height > screenHeight) {
				baseY = 0;
				baseX += width;
			}
			point.setLocation(baseX, baseY);
			baseY += height;

			try {
				point.start();
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i = 0; i < 3; i++) {
			ExitPoint point = new ExitPoint();

			point.config(hosts, ports);

			int width = point.getWidth() + 10;
			int height = point.getHeight() + 10;
			if (baseY + height > screenHeight) {
				System.out.println(baseY + " " + screenHeight);
				baseY = 0;
				baseX += width;
			}
			point.setLocation(baseX, baseY);
			baseY += height;

			try {
				point.start();
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
